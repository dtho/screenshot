#!/bin/dash

# shotsdir must be writable by cron user if a cron job
shotsdir=${HOME}/.shots

# maximum # of shots in shotsdir
shotsmax=200

# executable used to take the screenshot
screenshot_exe=import
screenshot_exe_args="-window root"

#
# sanity checks
#
if command -v ${screenshot_exe} &> /dev/null
then
    : # echo "Present"
else {
    echo "Could not find screenshot executable, ${screenshot_exe}"
    exit
}
fi

if [ ! -d $shotsdir ]
then {
    echo "shotsdir does not specify a directory"
    exit
}
fi

if [ -w $shotsdir ]
then
    : # echo "WRITABLE"
else {
    echo "NOT WRITABLE"
    exit
}
fi

# If running as a cron job, this likely is not set
# - grab first display value
display=`w $(id -un) | awk 'NF > 7 && $2 ~ /tty[0-9]+/ {print $3; exit}'`
export DISPLAY="${display}"


# randomly decide whether to take the screnshot
# random # between 0 and 255
random=`od -An -N1 -tu1 /dev/random`
random_cutoff=120
if [ $random -lt $random_cutoff ]
then {
    # take screenshot
    now=`date '+%F_%H:%M:%S'`
    ${screenshot_exe} ${screenshot_exe_args} $shotsdir/$now.png    
    # maintain dir - delete oldest once cap is reached
    n_files=`ls ${shotsdir} | wc -l`
    if [ $n_files -gt $shotsmax ]
    then {
	# delete a few files
	n_to_delete=5
	find ${shotsdir} -maxdepth 1 -type f | sort -R | tail -$n_to_delete |while read file; do
	    echo $file
	    # Something involving $file, or you can leave
	    # off the while to just get the filenames
	done
    }
    fi
}
fi
